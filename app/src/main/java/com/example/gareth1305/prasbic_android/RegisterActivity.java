package com.example.gareth1305.prasbic_android;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class RegisterActivity extends AppCompatActivity {

    Button registerBtn;
    TextView emailTxtView, passwordTxtView, firstNameTxtView, lastNameTxtView, emergencyContactNumTxtView;
    private GestureDetectorCompat gestureDetectorCompat;

    private TextWatcher textWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            checkFieldsForEmptyValues();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        gestureDetectorCompat = new GestureDetectorCompat(this, new MyGestureListener());

        emailTxtView = (TextView) findViewById(R.id.userEmailEditTxt);
        passwordTxtView = (TextView) findViewById(R.id.userPasswordEditTxt);
        firstNameTxtView = (TextView) findViewById(R.id.userFirstNameEditTxt);
        lastNameTxtView = (TextView) findViewById(R.id.lastNameEditText);
        emergencyContactNumTxtView = (TextView) findViewById(R.id.emergencyContactNumEditTxt);

        emailTxtView.addTextChangedListener(textWatcher);
        passwordTxtView.addTextChangedListener(textWatcher);
        firstNameTxtView.addTextChangedListener(textWatcher);
        lastNameTxtView.addTextChangedListener(textWatcher);
        emergencyContactNumTxtView.addTextChangedListener(textWatcher);
        checkFieldsForEmptyValues();


        registerBtn.setEnabled(false);
        registerBtn = (Button) findViewById(R.id.register);
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailToSubmit, passwordToSubmit,
                        firstNameToSubmit, lastNameToSubmit, emergencyContactNumToSubmit;
                emailToSubmit = emailTxtView.getText().toString();
                passwordToSubmit = passwordTxtView.getText().toString();
                firstNameToSubmit = firstNameTxtView.getText().toString();
                lastNameToSubmit = lastNameTxtView.getText().toString();
                emergencyContactNumToSubmit = emergencyContactNumTxtView.getText().toString();
                new RegisterUser().execute(emailToSubmit,
                        passwordToSubmit,
                        firstNameToSubmit,
                        lastNameToSubmit,
                        emergencyContactNumToSubmit);
                SharedPreferences preferences = getSharedPreferences("LOGIN_INFO", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("EMAIL", emailToSubmit);
                editor.putString("FIRSTNAME", firstNameToSubmit);
                editor.putString("LASTNAME", lastNameToSubmit);
                editor.putString("PASSWORD", passwordToSubmit);
                editor.putString("EMERGENCYCONTACT", emergencyContactNumToSubmit);
                editor.commit();
                Intent intent = new Intent(RegisterActivity.this,
                        ViewReportsActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.gestureDetectorCompat.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2,
                               float velocityX, float velocityY) {
            if(event2.getX() > event1.getX()){
                Intent intent = new Intent(
                        RegisterActivity.this, MainActivity.class);
                startActivity(intent);
            }
            return true;
        }
    }

    private void checkFieldsForEmptyValues() {
        registerBtn = (Button) findViewById(R.id.register);
        String email = emailTxtView.getText().toString();
        String pass = passwordTxtView.getText().toString();
        String fName = firstNameTxtView.getText().toString();
        String lName = lastNameTxtView.getText().toString();
        String emergencyContactNum = emergencyContactNumTxtView.getText().toString();

        if (email.length() > 0 && pass.length() > 0 &&
                fName.length() > 0 && lName.length() > 0 &&
                emergencyContactNum.length() > 0) {
            registerBtn.setEnabled(true);
        } else {
            registerBtn.setEnabled(false);
        }
    }

    private class RegisterUser extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            BufferedReader inBuffer = null;
            String url = "http://192.168.43.163:8080/create_user";
            String result = "fail";

            String email = params[0];
            String password = params[1];
            String firstName = params[2];
            String lastName = params[3];
            String emergencyContactNum = params[4];
            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost request = new HttpPost(url);
                List<NameValuePair> postParameters =
                        new ArrayList<NameValuePair>();
                postParameters.add(new BasicNameValuePair("email", email));
                postParameters.add(new BasicNameValuePair("emergencyContactNum", emergencyContactNum));
                postParameters.add(new BasicNameValuePair("firstName", firstName));
                postParameters.add(new BasicNameValuePair("lastName", lastName));
                postParameters.add(new BasicNameValuePair("password", password));
                UrlEncodedFormEntity formEntity =
                        new UrlEncodedFormEntity(postParameters);

                request.setEntity(formEntity);
                HttpResponse httpResponse = httpClient.execute(request);
                inBuffer = new BufferedReader(new InputStreamReader(
                        httpResponse.getEntity().getContent()));

                StringBuffer stringBuffer = new StringBuffer("");
                String line = "";
                String newLine = System.getProperty("line.separator");
                while ((line = inBuffer.readLine()) != null) {
                    stringBuffer.append(line + newLine);
                }
                inBuffer.close();
                result = stringBuffer.toString();
            } catch (Exception e) {
                result = e.getMessage();
            } finally {
                if (inBuffer != null) {
                    try {
                        inBuffer.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return result;
        }
    }
}
